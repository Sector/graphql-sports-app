const graphqlHTTP = require('express-graphql')
const { buildSchema } = require('graphql')
const fs = require('fs')
const resolvers = require('../controllers/resolvers').resolvers
const schema = fs.readFileSync(__dirname + '/../db/schema.graphql', 'utf8')

function init(app, passport) {
    /******* FACEBOOK *******/
    app.get('/auth/facebook',
        passport.authenticate('facebook'))

    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', { failureRedirect: '/login' }),
        function (req, res) {
            // Successful authentication, redirect to api.
            res.redirect('/api')
        })
    /******* END FACEBOOK *******/

    /******* GOOGLE *******/
    // GET /auth/google
    //   Use passport.authenticate() as route middleware to authenticate the
    //   request.  The first step in Google authentication will involve
    //   redirecting the user to google.com.  After authorization, Google
    //   will redirect the user back to this application at /auth/google/callback
    app.get('/auth/google',
        passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login'] }))

    // GET /auth/google/callback
    //   Use passport.authenticate() as route middleware to authenticate the
    //   request.  If authentication fails, the user will be redirected back to the
    //   login page.  Otherwise, the primary route function function will be called,
    //   which, in this example, will redirect the user to the api page.
    app.get('/auth/google/callback',
        passport.authenticate('google', { failureRedirect: '/login' }),
        function (req, res) {
            res.redirect('/api')
        })
    /******* END GOOGLE *******/

    function checkLoggedIn(req, res, next) {
        if (req.isAuthenticated()) {
            // req.user is available for use here
            return next()
        }
        // denied. redirect to login
        res.redirect('/login')
    }

    app.get('/', function (req, res) {
        res.send('home page')
    })

    app.get('/login', function (req, res) {
        res.send('login page')
    })

    app.get('/logout', (req, res, next) => {
        req.logout()
        res.redirect('/')
    })

    /*** PROTECTED URLs ***/
    app.use(checkLoggedIn)

    app.use('/api', graphqlHTTP({
        schema: buildSchema(schema),
        rootValue: resolvers,
        graphiql: true
    }))
}
exports.init = init