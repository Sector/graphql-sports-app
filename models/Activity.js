const MongooseSchema = require('../db/db').MongooseSchema
const mongoose = require('../db/db').mongoose

const activitySchema = new MongooseSchema(
    {
        user: { type: MongooseSchema.Types.ObjectId, ref: 'User' },
        date: Date,//and time
        type: String,
        duration: Number,
        calories: Number,
    })
const Activity = mongoose.model('Activity', activitySchema)
exports.Activity = Activity
