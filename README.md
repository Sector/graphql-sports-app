# Graphl-sports-app API

See other sections

## How to run
```
npm install
npm start
# To stop mongoDB after testing is finished run:
npm stop
```
Server is now running at port 4000 and mongoDB at 27018

## Using the API

Now you can navigate to http://localhost:4000/api or to
http://localhost:4000/auth/facebook if your access is forbidden.
(http://localhost:4000/auth/google can be used to google auth instead)

After the user is created get its id from mongo or by running:

```
query{
  users{
    _id
  }
}
```

And use the id of the user, to create user's details:

```
mutation{
  setUser(id: ${USER_ID}, weight: 60, height: 150, activityLevel:5,age:23,gender:Male)
  {
    name
  }
}
```

Use addActivity mutation to add activity:

```
mutation{
  addActivity(date:"2018-07-10", type:Running, duration:3060, user:${USER_ID})
  {
    activity{
      calories
    }
  }
}
```

Display details about a user and all its activities(pagination):

```
query{
  userSummary(id:${USER_ID)
  {
    activities(first:3, type:Running){
      edges{
        cursor
        node{
          date
          type
          duration
          calories
        }
      }
      totalCount
      totalBurned
      pageInfo{
        hasNextPage
        endCursor
      }
    }
  }
}
```

## Directory Structure
```
.
├── models
│   ├ Activity.js
│   ├ User.js
├── controllers
│   ├ resolvers.js
├── routes
│   ├ routes.js
│   ├ passport.js
├── db
│   ├ db.js
│   ├ schema.graphql
├ counting.js
├ index.js
├ package.json
├ .gitignore
```
