const caloriesCounter = require('../counting').caloriesCounter
const Activity = require('../models/Activity').Activity
const User = require('../models/User').User

function getActivitiesByDateRange(dateMin = null, dateMax = null){
    let date = {}
    try {
        if (dateMin) {
            let dateStart = new Date(dateMin).setHours(0, 0, 0, 0)
            date.$gte = dateStart
        }
        if (dateMax) {
            let dateEnd = new Date(dateMax).setHours(23, 59, 59, 59)
            date.$lte = dateEnd
        }
    } catch (err) {
        reject(err)
    }
    return date
}

const resolvers = {
    userSummary: (uargs) => {
        return {
            user: User.findById(uargs.id),
            activities: (aargs) => {
                let where = { user: uargs.id }
                let limit = null
                let sort = { date: 'asc' }
                if (aargs.type)
                    where.type = aargs.type
                if (aargs.first)
                    limit = aargs.first
                if (aargs.last) {
                    limit = aargs.last
                    sort.date = 'desc'
                }
                if (aargs.after)
                    where._id = { $gte: aargs.after }
                if (aargs.before)
                    where._id = { $lte: aargs.before }
                if (aargs.date)
                    where.date = getActivitiesByDateRange(aargs.date, aargs.date)
                if (aargs.dateMin || aargs.dateMax)
                    where.date = getActivitiesByDateRange(aargs.dateMin, aargs.dateMax)
                const edgesPromise = Activity.find(where)
                    .limit(limit)
                    .sort(sort)
                    .populate('user')
                return new Promise((resolve, reject) => {
                    edgesPromise.exec((err, docs) => {
                        if (err)
                            reject(err)
                        let edges = docs.map(x => ({cursor: x.id, node: x}))
                        let totalCount = limit == null ? docs.length : limit
                        let pageInfo = {
                            endCursor: docs.length ? docs[docs.length - 1].id : null,
                            hasNextPage: new Promise((resolve, reject) => {
                                Activity.count(where, (err, count) => {
                                    if (err)
                                        reject(err)
                                    else
                                        ((count - limit) > 0) ? resolve(true) : resolve(false)
                                }).sort(sort)

                            })
                        }
                        let totalBurned = docs.reduce(function(acc, curr){
                                return acc + curr.calories
                            }, 0)
                        resolve({
                            edges: edges,
                            totalCount: totalCount,
                            pageInfo: pageInfo,
                            totalBurned: totalBurned,
                        })
                    })
                })
            },

        }
    },

    getRecommendedCaloryIntake(args) {
        new Promise(function (resolve, reject) {
            User.findById(args.id, (err, user) => {
                if (err)
                    reject(err)
                resolve(caloriesCounter.getRecommendedCaloryIntake(user))
            })
        })
    }
}

exports.resolvers = resolvers
