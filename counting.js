class CaloriesCounting {
    constructor() {
        // http://www.nutristrategy.com/caloriesburned.htm 
        this.weightCategories = [58.967, 70.306, 81.647, 92.986]
        this.activities = {
            Running: [472, 563, 654, 745],
            Basketball: [354, 422, 490, 558],
            Soccer: [106, 126, 147, 167]
        }

    }

    getPosition(weight) {
        let i = 0
        for (; i < this.weightCategories.length; i++) {
            console.log(i)
            if (i == this.weightCategories.length)
                return i
            if (weight < this.weightCategories[i])
                break
            console.log(i)
        }
        return i
    }

    getCalories(weight, activity, duration) {
        /* 
        *weight* of the person
        *activity* array of burned calories 
        *duration* of the activity in seconds
        */
        activity = this.activities[activity]

        let pos = this.getPosition(weight)
        if (pos == this.weightCategories.length || pos === 0)
            return Math.round(activity[pos] * (duration / 60 / 60))
        else {
            let weightInt = this.weightCategories[pos] - this.weightCategories[pos - 1]
            let weightPos = (weight - this.weightCategories[pos - 1]) / weightInt
            let actInt = activity[pos] - activity[pos - 1]
            return Math.round((activity[pos - 1] + weightPos * actInt) * (duration / 60 / 60))
        }
    }

    getRecommendedCaloryIntake(user) {
        // Mifflin – St Jeor Formula
        if (user.gender == "Female")
            return 10 * user.weight + 6.25 * user.height - 5 * user.age - 161
        return 10 * user.weight + 6.25 * user.height - 5 * user.age + 5
    }

    secondsTillMidnight(time) {
        var midnight = new Date(time)
        midnight.setHours(24, 0, 0, 0)
        return (midnight.getTime() - (time.getTime())) / 1000
    }

    getRecommendedCaloryIntake(args) {
        new Promise(function (resolve, reject) {
            User.findById(args.id, (err, user) => {
                if (err)
                    reject(err)
                resolve(caloriesCounter.getRecommendedCaloryIntake(user))
            })
        })
    }
}

exports.caloriesCounter = new CaloriesCounting()