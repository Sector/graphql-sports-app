const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const User = require('../models/User').User

function init(){
    /*** AUTH ***/
    passport.use(new FacebookStrategy({
        clientID: "427779377740020",
        clientSecret: "bfb3872ed5a9b31aa3068d0423fd49c5",
        callbackURL: "http://localhost:4000/auth/facebook/callback"
    },
    function(accessToken, refreshToken, profile, done) {
        User.findOne({ facebookId: profile.id }, function (err, user) {
        if (err)
            return err
        if (user)
            return done(err, user);
        else
            {
            user = new User({
                facebookId: profile.id,
                token: accessToken,
                name: profile.displayName,
            })
            user.save(function(err){
                if (err)
                return err
                return done(null, user)
            });
            }
        });
    }
    ));

    passport.use(new GoogleStrategy({
        clientID: "696984035877-h58me9no4sjp8poifgnpds43t970bfnt.apps.googleusercontent.com",
        clientSecret: "zhCncqk_Ks4nXS2eA6I0t6mJ",
        callbackURL: "http://localhost:4000/auth/google/callback"
    },
    function(accessToken, refreshToken, profile, done) {
        User.findOne({ googleId: profile.id }, function (err, user) {
            if (err)
            return err
            if (user){
            return done(err, user);
            }
            else
            {
                user = new User({
                googleId: profile.id,
                token: accessToken,
                name: profile.displayName
                })
                user.save(function(err){
                if (err)
                    return err
                return done(null, user)
                });
            }
            });
    }
    ));
}

exports.init = init