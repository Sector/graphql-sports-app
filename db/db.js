const mongoose = require('mongoose');
DB_URL = 'localhost:27018'
DB_NAME = 'graphql'
mongoose.connect('mongodb://' + DB_URL + '/' + DB_NAME);

exports.MongooseSchema = mongoose.Schema;
exports.mongoose = mongoose;