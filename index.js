const express = require('express');
const passport = require('passport');
const app = express();
const PORT = 4000;


passport.serializeUser(function (user, cb) {
  cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
  cb(null, obj);
});

app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
// FIXME: API gets stuck when body-parser is used
//app.use(require('body-parser').urlencoded({ extended: true }));
app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));

// Initialize Passport and restore authentication state, if any, from the
// session.
app.use(passport.initialize());
app.use(passport.session());

/****** ROUTES ******/
require('./routes/passport').init()
require('./routes/routes').init(app, passport)

// run
app.listen(PORT, () => console.log('The server is running on port:' + PORT));