let User = require('../models/User').User
let UserResolvers = require('./user').resolvers
let ActivityResolvers = require('./activity').resolvers
let caloriesCounter = require('../counting').caloriesCounter



const resolvers = {
  // Queries
  users: () => User.find(),
  userSummary: (args) => UserResolvers.userSummary(args),
  recommendedCaloryIntake: (args) => UserResolvers.recommendedCaloryIntake(args),

  // Mutations
  setUser: (args) => User.findByIdAndUpdate(args.id, args, { new: true, upsert: true }),
  createUser: (args) => User.findByIdAndUpdate(args.id, args, { new: true, upsert: true }),
  addActivity: (args) => ActivityResolvers.addActivity(args.date, args.type, args.duration, args.user)
}

exports.resolvers = resolvers
