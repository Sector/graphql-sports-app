const caloriesCounter = require('../counting').caloriesCounter
const User = require('../models/User').User
const Activity = require('../models/Activity').Activity

const resolvers = {
    addActivity: (date, type, duration, userID) => {
        return new Promise((resolve, reject) => {
            User.findById(userID, (err, user) => {
                if (err)
                    reject(err)
                try {
                    let date1 = new Date(date)
                    let calories = caloriesCounter.getCalories(user.weight, type, duration)
                    let tillMidnight = caloriesCounter.secondsTillMidnight(date1)
                    if (duration <= tillMidnight) {
                        let ac = new Activity({
                            date: date1,
                            type: type,
                            duration: duration,
                            calories: calories,
                            user: user.id
                        })
                        ac.save()
                        resolve({
                            activity: [ac],
                            user: user
                        })
                    } else {
                        let calories1 = caloriesCounter.getCalories(user.weight, type, tillMidnight)
                        let ac1 = new Activity({
                            date: date1,
                            type: type,
                            duration: tillMidnight,
                            calories: calories1,
                            user: user.id,
                        })
                        ac1.save()

                        let calories2 = caloriesCounter.getCalories(user.weight, type, duration - tillMidnight)
                        // Need new date variable because data from the old one might not have been saved
                        let date2 = new Date(date).setHours(24, 0, 0, 0)
                        let ac2 = new Activity({
                            date: date2,
                            type: type,
                            duration: duration - tillMidnight,
                            calories: calories2,
                            user: user.id,
                        })
                        ac2.save()
                        resolve({
                            activity: [ac1, ac2],
                            user: user
                        })
                    }
                }
                catch (err) {
                    reject(err)
                }
            })

        })
    },
}

exports.resolvers = resolvers